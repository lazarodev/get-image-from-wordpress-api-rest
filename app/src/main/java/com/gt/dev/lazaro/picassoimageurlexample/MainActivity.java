package com.gt.dev.lazaro.picassoimageurlexample;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.gt.dev.lazaro.picassoimageurlexample.adapter.BlogAdapter;
import com.gt.dev.lazaro.picassoimageurlexample.adapter.BlogGS;
import com.gt.dev.lazaro.picassoimageurlexample.controller.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    protected final String urlPosts = "http://dev.lazarusgt.com/wp-json/wp/v2/posts";
    private RecyclerView rvPosts;
    private ProgressDialog progressDialog;
    private ArrayList<BlogGS> blog = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startVars();
        showPosts();
    }

    /**
     * Start all your variables and initialize them.
     */
    private void startVars() {
        rvPosts = findViewById(R.id.rv_main);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvPosts.setLayoutManager(layoutManager);
        rvPosts.setHasFixedSize(true);
        rvPosts.setItemAnimator(new DefaultItemAnimator());
        // This is the typical "Loading Message" for the users
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading posts...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    /**
     * setUp our adapter right here!
     *
     * @param posts
     */
    private void setUpAdapter(ArrayList<BlogGS> posts) {
        this.rvPosts.setAdapter(new BlogAdapter(posts));
    }

    /**
     * In this method we'll show the list from Wordpress entries
     */
    private void showPosts() {
        // Request with direction to our urlPosts constant
        JsonArrayRequest req = new JsonArrayRequest(urlPosts, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("POSTS", response.toString());

                try {
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject post = (JSONObject) response.get(i);
                        JSONObject postTitle = post.getJSONObject("title");
                        JSONObject betterFeatured = post.getJSONObject("better_featured_image");
                        JSONObject mediaDetails = betterFeatured.getJSONObject("media_details");
                        JSONObject sizes = mediaDetails.getJSONObject("sizes");

                        JSONObject image = sizes.getJSONObject("medium");

                        String urlImage = image.getString("source_url");

                        int postId = post.getInt("id");
                        String title = postTitle.getString("rendered");

                        blog.add(new BlogGS(title, urlImage, postId));
                        setUpAdapter(blog);
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("LOG VOLLEY", "Error: " + error.getMessage());
                progressDialog.dismiss();
            }
        });
        // We set our max DEFAULT intents to get our response
        RetryPolicy policy = new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(req);
    }

}
