package com.gt.dev.lazaro.picassoimageurlexample.adapter;

/**
 * Created by LazaroDev on 5/12/2017.
 */

public class BlogGS {

    private String title, image;
    private int id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BlogGS(String title, String image, int id) {
        this.title = title;
        this.image = image;
        this.id = id;
    }

}
