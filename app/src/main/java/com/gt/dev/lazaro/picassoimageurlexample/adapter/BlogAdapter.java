package com.gt.dev.lazaro.picassoimageurlexample.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.gt.dev.lazaro.picassoimageurlexample.R;
import com.gt.dev.lazaro.picassoimageurlexample.controller.AppController;

import java.util.List;

/**
 * Created by LazaroDev on 5/12/2017.
 */

public class BlogAdapter extends RecyclerView.Adapter<BlogAdapter.MyViewHolder> {

    private List<BlogGS> postsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle;
        public NetworkImageView niBlog;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tv_blog);
            niBlog = view.findViewById(R.id.ni_blog);
        }

    }

    public BlogAdapter(List<BlogGS> postList) {
        this.postsList = postList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.blog_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BlogGS blogGS = postsList.get(position);
        holder.tvTitle.setText(blogGS.getTitle());
        holder.niBlog.setImageUrl(blogGS.getImage(), AppController.getInstance().getImageLoader());
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }
}
